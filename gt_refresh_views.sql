IF EXISTS (SELECT [NAME] FROM [sys].[triggers] WHERE Name = N'GT_REFRESH_VIEWS')
	DROP TRIGGER [GT_REFRESH_VIEWS] ON DATABASE
GO

CREATE TRIGGER GT_REFRESH_VIEWS ON DATABASE
FOR ALTER_TABLE
/* Copyright 2020                GazIntech LLC              info@gazintech.ru */
/* Usage of the works is permitted  provided that this instrument is retained */
/* with the works, so that any entity that uses the works is notified of this */
/* instrument.     DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.                */
AS
DECLARE @Event XML = EVENTDATA()
DECLARE @TableName VARCHAR(200) = @Event.value('(/EVENT_INSTANCE/ObjectName)[1]',  'nvarchar(128)') 
DECLARE @ViewName  VARCHAR(200)
DECLARE Cur CURSOR LOCAL STATIC
	FOR SELECT [NAME]
	FROM sys.views
	WHERE
		OBJECT_DEFINITION (OBJECT_ID([NAME])) LIKE '%' + @TableName + '%' 

OPEN Cur
    FETCH NEXT FROM Cur INTO @ViewName
    WHILE @@FETCH_STATUS = 0
    BEGIN
	   EXECUTE sp_refreshview @ViewName
	   FETCH NEXT FROM Cur INTO @ViewName 
	END
CLOSE Cur 
DEALLOCATE Cur  
